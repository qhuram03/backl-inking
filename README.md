# AUTO ACCIDENT CHIROPRACTOR #

RECOVERING FROM AN ACCIDENT INJURY: FINDING AN AUTO ACCIDENT CHIROPRACTOR AND PHYSICAL THERAPIST NEAR YOU​

An auto accident chiropractor and a physical therapist can work independently as well, but the treatment often yields better results for patients when both work in conjunction to provide customized therapy. A car accident chiropractor in specific, and chiropractic service in general, is differentiated with regards to the expertise in spinal manipulation and the musculoskeletal system. While chiropractic alignment improves the nervous system functions, including spinal cord and brain function, it also translates into a strengthening of the overall bodily function, movement and balance. All of which are necessary in restoring life to its fullest so you can live it up actively with your loved ones.

Physical therapy, on the other hand, focuses more broadly on the body and uses techniques such as muscle stretching, strength training, posture adjustment and other natural recovery modalities that provide permanent recovery.  In a similar vein, occupational therapy helps patients by relieving stress from daily activities, for instance, by suggesting work/home environment changes that can help them recover faster. Massage therapy is another remedy that is highly beneficial for accident injuries as it helps reduce muscle spasms, stimulates tissue healing, softens stiff scar tissue, increases flexibility, lowers anxiety and helps release the “happy” hormones (i.e. Endorphins and Serotonin).


### What is this repository for? ###

* Quick summary
* Version
* http://www.usptrehab.com/recovering-from-an-accident-injury.html


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact